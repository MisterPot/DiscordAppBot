import asyncio
import json
from functools import wraps
from typing import Callable, Dict, List

import requests
from disnake.ext import commands
import inspect
import os.path as path
import importlib

_root = path.dirname(__file__)
DB_PATH = path.join(_root, 'applications.json')
BOT_FILE = path.join(_root, 'bot.py')


def find_applications() -> List[Dict[str, str]]:
    with open(DB_PATH, 'r') as file:
        return json.load(file)


def load_applications(bot: commands.Bot) -> None:
    """
    Load applications , which was passed to file `applications.conf`
    :return:
    """
    for application in find_applications():
        app_name, app_path, app_enabled = application.values()

        if not app_enabled:
            continue

        app_class_name, module_path = [item[::-1] for item in app_path[::-1].split('.', maxsplit=1)]
        app_class = getattr(importlib.import_module(module_path), app_class_name)
        bot.add_cog(app_class(bot))

        print(f"Application with path `{module_path}` and app class `{app_class_name}` was added successfully")


def add_validators(validators: Dict[str, Callable]) -> Callable:
    """
    Add validators to functions
    :param validators: its dictionary with {key: validator_function(value_of_key)}
    if keys not in function, raises KeyError
    :return:
    """

    def func_wrapper(function: Callable) -> Callable:

        def inner_function(*args, **kwargs):
            function_arguments = inspect.signature(function).bind(*args, **kwargs)

            for key, value in function_arguments.arguments.items():
                if not (key in function_arguments.arguments.keys()):
                    raise KeyError(f'This function have not argument `{key}`')

                if not validators.get(key, lambda arg: True)(value):
                    raise KeyError(f'`{key}` argument is invalid')

        if asyncio.iscoroutinefunction(function):
            @wraps(function)
            async def arg_wrapper(*args, **kwargs):
                inner_function(*args, **kwargs)
                return await function(*args, **kwargs)

            return arg_wrapper

        @wraps(function)
        def arg_wrapper(*args, **kwargs):
            inner_function(*args, **kwargs)
            return function(*args, **kwargs)

        return arg_wrapper

    return func_wrapper


@add_validators({
    "url": lambda url: url.startswith('https') and (
            'youtube' in url or 'spotify.com' in url
    )
})
def is_valid_url(url: str) -> bool:
    """
    Is this url exists in YouTube
    :param url:
    :return:
    """
    return requests.get(url).ok
