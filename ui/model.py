import json
from utils import find_applications, DB_PATH


class Model:

    def __init__(self):
        self.applications = find_applications()

    def update(self) -> None:
        self.applications = find_applications()

    def application_set_enabled(self, application_name: str, enabled: bool) -> None:
        filtered = list(filter(
            lambda item: item['app_name'] == application_name, self.applications
        ))

        if not filtered:
            raise KeyError('No such application')

        filtered[0]['enabled'] = enabled

    def confirm(self) -> None:
        with open(DB_PATH, 'w') as file:
            file.write(json.dumps(self.applications, indent=4))