import sys
from typing import Dict, List, Callable
from pathlib import Path


from PyQt5.QtCore import Qt
from PyQt5.QtGui import QIcon
from PyQt5.QtWidgets import *
from PyQt5 import QtGui

from .model import Model


from PyQt5 import QtCore
import functools


@functools.lru_cache()
class GlobalObject(QtCore.QObject):
    def __init__(self):
        super().__init__()
        self._events = {}

    def addEventListener(self, name, func):
        if name not in self._events:
            self._events[name] = [func]
        else:
            self._events[name].append(func)

    def dispatchEvent(self, name: str, *args, **kwargs):
        functions = self._events.get(name, [])
        for func in functions:
            QtCore.QTimer.singleShot(0, lambda: func(*args, **kwargs))


class ApplicationItem(QListWidgetItem):

    @property
    def enabled(self) -> int:
        return self.checkState()

    @enabled.setter
    def enabled(self, value: int) -> None:
        if value:
            self.setCheckState(Qt.CheckState.Checked)
        else:
            self.setCheckState(Qt.CheckState.Unchecked)

    def __init__(
            self,
            app_name: str,
            app_class: str,
            enabled: bool = True
    ):
        super(ApplicationItem, self).__init__()
        self.app_name = app_name
        self.app_class = app_class
        self.enabled = enabled
        self.setText(f'{app_name:<30}  {app_class:>50}')


class ApplicationList(QListWidget):

    def __init__(self):
        super(ApplicationList, self).__init__()

        def item_clicked(item: ApplicationItem) -> None:
            item.enabled = False if item.enabled else True

        def item_selected(item: ApplicationItem) -> None:
            item_clicked(item)

        self.itemActivated.connect(item_selected)
        self.setItemAlignment(Qt.AlignmentFlag.AlignLeading)
        self.add_item_handler(item_clicked)

    def add_item(self, app_name: str, app_class: str, enabled: bool) -> None:
        item = ApplicationItem(app_name=app_name, app_class=app_class, enabled=enabled)
        self.addItem(item)

    def update_list(self, apps: List[Dict[str, str]]) -> None:
        # self.event()
        self.clear()
        self.add_items(apps=apps)

        GlobalObject().dispatchEvent('log', 'List is updated')

    def add_item_handler(self, function: Callable) -> None:
        self.itemClicked.connect(function)

    def add_items(self, apps: List[Dict[str, str]]) -> None:
        for app_data in apps:
            self.add_item(**app_data)


class BottomButtonMenu(QFrame):

    def __init__(self):
        super(BottomButtonMenu, self).__init__()
        self._layout = QHBoxLayout()
        self.update_button = QPushButton('Update apps')
        self.start_button = QPushButton('Start')
        self.stop_button = QPushButton('Stop')
        self.reload_button = QPushButton('Restart')
        self.logger = Logs()
        self.stop_button.setDisabled(True)
        self.reload_button.setDisabled(True)
        self._layout.addWidget(self.logger)
        self._layout.addWidget(self.update_button)
        self._layout.addWidget(self.start_button)
        self._layout.addWidget(self.stop_button)
        self._layout.addWidget(self.reload_button)
        self.setLayout(self._layout)


class Logs(QTextEdit):

    def __init__(self):
        super(Logs, self).__init__()
        GlobalObject().addEventListener('log', self.log)
        self.setReadOnly(True)

    def log(self, text: str) -> None:
        self.setText(self.toPlainText() + f"{text} \n")


class MainWindow(QWidget):

    def __init__(self, model: Model):
        super(MainWindow, self).__init__()
        self._layout = QVBoxLayout()
        self.app_list = ApplicationList()
        self.bottom_menu = BottomButtonMenu()

        def update_and_load(_):
            model.update()
            self.app_list.update_list(model.applications)

        def stop_button_handler(_):
            self.bottom_menu.stop_button.setDisabled(True)
            self.bottom_menu.start_button.setDisabled(False)

        def start_button_handler(_):
            self.bottom_menu.start_button.setDisabled(True)
            self.bottom_menu.stop_button.setDisabled(False)

        def list_item_handler(item):
            GlobalObject().dispatchEvent('log', f'Application `{item.app_name}` set enabled to {item.enabled}')
            model.application_set_enabled(item.app_name, item.enabled)
            model.confirm()

        self.bottom_menu.update_button.clicked.connect(update_and_load)
        self.bottom_menu.stop_button.clicked.connect(stop_button_handler)
        self.bottom_menu.start_button.clicked.connect(start_button_handler)
        self.app_list.add_item_handler(list_item_handler)

        self._layout.addWidget(self.app_list)
        self._layout.addWidget(self.bottom_menu)
        self.setLayout(self._layout)

    def keyPressEvent(self, event: QtGui.QKeyEvent) -> None:
        if event.key() == Qt.Key_Escape:
            self.close()


class View:

    def __init__(self, model: Model):
        super(View, self).__init__()
        self.model = model
        self.app = QApplication([])
        self.main_window = MainWindow(model=self.model)

    def mainloop(self) -> None:
        ico_file = Path(__file__).parent / Path('d-ico.ico')
        ico = QIcon()

        ico.addFile(str(ico_file))
        self.main_window.setWindowIcon(ico)
        self.main_window.setWindowTitle('Application Bot')
        self.main_window.app_list.add_items(self.model.applications)
        self.main_window.resize(700, 500)
        self.main_window.show()

        sys.exit(self.app.exec())
