from multiprocessing import Process

from .view import View, GlobalObject
from .model import Model
from utils import load_applications
from bot import bot, token
import logging


def with_dispatcher(function):
    def wrapper(level: int, msg: str, *args, **kwargs) -> None:
        function(level, msg, *args, **kwargs)
        GlobalObject.dispatchEvent('log', msg)
    return wrapper


loggers = [logging.getLogger(name) for name in logging.root.manager.loggerDict]


for logger in loggers:
    logger._log = with_dispatcher(logger._log)


class Controller:

    def __init__(self, model: Model, view: View):
        self.bot_process: Process = None
        self.view = view
        self.model = model

        self.view.main_window.bottom_menu.stop_button.clicked.connect(self.stop_bot)
        self.view.main_window.bottom_menu.start_button.clicked.connect(self.start_bot)
        self.view.main_window.bottom_menu.reload_button.clicked.connect(bot.clear)

    @staticmethod
    def run_bot() -> None:
        load_applications(bot)
        bot.run(token)

    def start_bot(self) -> None:
        self.bot_process = Process(target=self.run_bot)
        self.bot_process.start()
        GlobalObject().dispatchEvent('log', f"""
            Bot ran by process - {self.bot_process.pid}
            Bot process name name - `{self.bot_process.name}`
        """)

    def stop_bot(self) -> None:
        self.bot_process.terminate()
        self.bot_process.join()
        GlobalObject().dispatchEvent('log', f"""
            Bot is turned off with exitcode {self.bot_process.exitcode}
        """)
        self.bot_process.close()

    def run(self) -> None:
        self.view.mainloop()
