import disnake
from disnake.ext import commands


class ExtCog(commands.Cog):

    def __init__(self, bot: commands.Bot):
        self.bot = bot

    @commands.slash_command(description='Shows message as you')
    async def eme(self, inter: disnake.ApplicationCommandInteraction, msg: str):
        await inter.send(f"# <@{inter.user.id}> {msg}")

    @commands.slash_command(description='Shutdowns server')
    @commands.has_role('Адмін')
    async def shutdown(self, inter: disnake.ApplicationCommandInteraction):
        await inter.response.send_message(ephemeral=True, content="Shutting down ...")
        await self.bot.close()
