from __future__ import annotations

from typing import Dict
import aiohttp
import disnake
from disnake.ext import commands
from disnake.utils import get

from .quiz import Quiz
from .utils import on_quiz_not_found
from utils import add_validators
from .qzfile import QZFile
from .view import CategoryView


class QuizCog(commands.Cog):

    def __init__(self, bot: commands.Bot):
        self.bot = bot
        self.quiz_channels: Dict[int, Dict[str, disnake.TextChannel | Quiz]] = {}

    @commands.Cog.listener()
    async def on_ready(self):
        for guild in self.bot.guilds:
            if not get(guild.categories, name='Quiz'):
                await guild.create_category('Quiz')

    @commands.Cog.listener()
    async def on_slash_command_error(self, inter: disnake.ApplicationCommandInteraction, error):
        await inter.response.send(str(error))

    @commands.slash_command()
    async def quiz(self, inter: disnake.ApplicationCommandInteraction):
        pass

    @quiz.sub_command(description='Create quiz channel from `.qz` file')
    @add_validators({
        "file": lambda f: f.filename.endswith('.qz') or print(f.url) or print(f.filename)
    })
    async def create(self, inter: disnake.ApplicationCommandInteraction, file: disnake.Attachment):

        if self.quiz_channels.get(inter.user.id):
            raise KeyError("You already have a 1 quiz channel. Delete it first")

        channel_name = f"{inter.user.name}_{file.filename.split('.')[0]}"
        channel = await get(inter.guild.categories, name='Quiz').create_text_channel(channel_name)

        async with aiohttp.request('get', file.url) as resp:
            content = await resp.read()

        await channel.set_permissions(
            inter.user.roles[0], start_embedded_activities=False, view_channel=True,
            send_messages=False, use_slash_commands=False, use_application_commands=False
        )
        await channel.set_permissions(
            inter.user, send_messages=True, start_embedded_activities=True, use_slash_commands=True
        )

        try:
            self.quiz_channels[inter.user.id] = {'channel': channel, 'quiz': QZFile.decode_quiz(content)}
        except OverflowError:
            raise TypeError('Incorrect `.qz` file')

        await inter.response.send_message(f'Channel created successfully - `#{channel_name}`')

    @quiz.sub_command(description='Starts quiz in channel')
    @on_quiz_not_found
    async def start(self, inter: disnake.ApplicationCommandInteraction):
        ch_data = self.quiz_channels[inter.user.id]
        await inter.send(content='Categories : ', view=CategoryView(ch_data['quiz']))

    @quiz.sub_command(description="Stops current quiz")
    @on_quiz_not_found
    async def stop(self, inter: disnake.ApplicationCommandInteraction):
        pass

    @quiz.sub_command(description='Add judge to quiz')
    @on_quiz_not_found
    async def add_judge(self, inter: disnake.ApplicationCommandInteraction, member: disnake.Member):
        last_judge = self.quiz_channels[inter.user.id]['quiz'].judge
        channel = self.quiz_channels[inter.user.id]['channel']

        if last_judge:
            await channel.set_permissions(
                member, use_slash_commands=False, send_messages=False, start_embedded_activities=False
            )

        self.quiz_channels[inter.user.id]['quiz'].judge = member

        await channel.set_permissions(
            member, use_slash_commands=True, send_messages=True, start_embedded_activities=True
        )
        await inter.response.send_message(f'Judge changed to - > {member.name}')

    @quiz.sub_command(description='Adds new player to quiz')
    @on_quiz_not_found
    async def add_player(self, inter: disnake.ApplicationCommandInteraction, member: disnake.Member):
        self.quiz_channels[inter.user.id]['quiz'].players.add(member)
        self.quiz_channels[inter.user.id]['quiz'].players_scores[member] = 0
        await self.quiz_channels[inter.user.id]['channel'].set_permissions(
            member, start_embedded_activities=True
        )
        await inter.response.send_message(f'Player successfully added - {member}')

    @quiz.sub_command(description='Delete player from quiz')
    @on_quiz_not_found
    async def remove_player(self, inter: disnake.ApplicationCommandInteraction, member: disnake.Member):
        try:
            self.quiz_channels[inter.user.id]['quiz'].players.remove(member)
            self.quiz_channels[inter.user.id]['quiz'].players_scores.pop(member)
            await self.quiz_channels[inter.user.id]['channel'].set_permissions(
                member, start_embedded_activities=False
            )
            await inter.response.send_message(f'Player deleted - {member}')
        except KeyError:
            await inter.response.send_message(f'Player not in quiz - {member.name}')

    @quiz.sub_command(description='Deletes quiz channel')
    @on_quiz_not_found
    async def delete(self, inter: disnake.ApplicationCommandInteraction):

        ch_data = self.quiz_channels.get(inter.user.id)
        if not ch_data:
            raise KeyError('You haven\' quiz channel')

        self.quiz_channels[inter.user.id] = {}

        await ch_data['channel'].delete()
        await inter.response.send_message(f"Channel successfully deleted - {ch_data['channel'].name}")

    @quiz.sub_command(description='Shows current score')
    @on_quiz_not_found
    async def score(self, inter: disnake.ApplicationCommandInteraction):
        players_scores = self.quiz_channels[inter.user.id]['quiz'].players_scores
        await inter.response.send_message(
            "Scores : "
            ' - '.join(f"{user.name} - {score}" for user, score in players_scores.items())
        )
