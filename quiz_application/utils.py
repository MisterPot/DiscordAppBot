import random
from functools import wraps
from typing import Callable
import disnake


def on_quiz_not_found(function: Callable) -> Callable:
    """
    If quiz not found raises KeyError
    :param function:
    :return:
    """
    @wraps(function)
    async def arg_wrapper(self, inter: disnake.ApplicationCommandInteraction, *args, **kwargs):

        ch_data = self.quiz_channels.get(inter.user.id, {})
        channel = ch_data.get('channel')

        if not ch_data or (channel and channel.name != inter.channel.name):
            raise KeyError('This channel not contain quiz')
        return await function(self=self, inter=inter, *args, **kwargs)
    return arg_wrapper


def random_button_style() -> disnake.ButtonStyle:
    return random.choice([
        disnake.ButtonStyle.green,
        disnake.ButtonStyle.link,
        disnake.ButtonStyle.secondary,
        disnake.ButtonStyle.blurple,
        disnake.ButtonStyle.danger,
        disnake.ButtonStyle.gray,
        disnake.ButtonStyle.grey,
        disnake.ButtonStyle.primary,
        disnake.ButtonStyle.red,
        disnake.ButtonStyle.success
    ])
