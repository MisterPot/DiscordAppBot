from __future__ import annotations

import math
from quiz_application.quiz import Quiz
import yaml


class QZFile:

    @staticmethod
    def decode_quiz(bytes_to_decode: bytes) -> Quiz:
        return Quiz.from_dict(yaml.load(
            QZFile.decode_bytes(bytes_to_decode), yaml.CLoader
        ))

    @staticmethod
    def encode_quiz(quiz: Quiz) -> bytes:
        # noinspection PyTypeChecker
        yaml_f: str = yaml.dump(quiz.to_dict(), indent=4)
        return QZFile.encode_string(yaml_f)

    @staticmethod
    def decode_bytes(_bytes: bytes) -> str:

        def decode_to_symbol(_bytes: bytes) -> str:
            return chr(math.floor(math.sqrt(int.from_bytes(_bytes, 'little') * 4)))

        return ''.join(map(decode_to_symbol, _bytes.split(b'\xff')))

    @staticmethod
    def encode_string(line: str) -> bytes:

        def encode_symbol(sym: str) -> bytes:
            in_num = math.ceil((ord(sym) ** 2) / 4)
            return int.to_bytes(in_num, math.ceil(in_num / 255) or 1, 'little')

        return b'\xFF'.join(map(encode_symbol, line))
