from __future__ import annotations
import dataclasses
from typing import Dict, Set
import yaml
from collections import deque
import disnake


DEFAULT_REWARD = 100
DEFAULT_DIFFICULTY = 1


@dataclasses.dataclass
class Question:
    reward: int
    correct: str
    description: str
    difficulty: int

    def to_dict(self) -> dict:
        return {
            'reward': self.reward,
            'correct': self.correct,
            'description': self.description,
            'difficulty': self.difficulty
        }

    @staticmethod
    def from_dict(d: dict) -> Question:
        return Question(
            reward=d.get('reward', DEFAULT_REWARD),
            correct=d.get('correct', "Nothing"),
            description=d.get('description', 'Nothing'),
            difficulty=d.get('difficulty', DEFAULT_DIFFICULTY)
        )

    def __hash__(self):
        return hash((self.difficulty, self.description, self.reward, self.correct))


@dataclasses.dataclass
class Category:
    name: str
    questions: Set[Question]

    def to_dict(self) -> dict:
        return {
            'name': self.name,
            'questions': [q.to_dict() for q in self.questions]
        }

    @staticmethod
    def from_dict(d: dict) -> Category:
        return Category(
            name=d.get('name', 'None'),
            questions={Question.from_dict(q_item) for q_item in d.get('questions', [])}
        )

    def __hash__(self):
        return hash((self.name, tuple(ques for ques in self.questions)))


@dataclasses.dataclass
class Quiz:
    players: set = dataclasses.field(default_factory=set)
    judge: disnake.User = None
    categories: Set[Category] = dataclasses.field(default_factory=list)
    players_scores: Dict[disnake.Member, int] = dataclasses.field(default_factory=dict)

    def __post_init__(self):
        self.que = deque(self.categories)

    def remove_question(self, p_question: Question):
        """
        Find this question in categories and removes it
        :param p_question:
        :return:
        """

        for category in self.categories:
            for question in category.questions:
                if question is p_question:
                    category.questions.discard(question)
                    break

    def pop_empty_category(self) -> None:
        """
        If category is empty just removes this
        :return:
        """
        for category in self.categories:
            if not category.questions:
                self.categories.discard(category)
                break

    def to_dict(self) -> dict:
        return {
            'categories': [c.to_dict() for c in self.categories]
        }

    @staticmethod
    def from_dict(d: dict) -> Quiz:
        return Quiz(categories={
            Category.from_dict(c) for c in d.get('categories')
        })

    def dump_to_yaml(self, filename: str, indent: int = 2) -> None:
        with open(filename, 'w') as file:
            file.write(yaml.dump(self.to_dict(), indent=indent))
