from __future__ import annotations

import disnake.components

import quiz_application.components as components
from .utils import random_button_style
from .quiz import Category, Quiz, Question


class NoTimeoutView(disnake.ui.View):

    def __init__(self):
        super(NoTimeoutView, self).__init__(timeout=None)

    @classmethod
    def get_id(cls, uid: str | int) -> str:
        return f"{cls.__name__}:{uid}"


class CategoryView(NoTimeoutView):

    def __init__(self, quiz: Quiz):
        super(CategoryView, self).__init__()
        self.quiz = quiz
        self.prepare()

    def prepare(self) -> CategoryView:
        id_ = 0
        for category in self.quiz.categories:
            id_ += 1
            self.add_item(components.ViewButton(
                label=category.name, style=random_button_style(),
                view=QuestionView, view_params={"category": category, "quiz": self.quiz},
                inter_params={'content': "Questions : (question reward:difficulty level)"}
            ))
        return self


class QuestionView(NoTimeoutView):

    def __init__(self, category: Category, quiz: Quiz):
        super(QuestionView, self).__init__()
        self.category = category
        self.quiz = quiz
        self.prepare()

    def prepare(self) -> QuestionView:
        id_ = 0
        for question in self.category.questions:
            id_ += 1
            self.add_item(components.QuestionButton(
                label=f"{question.reward}: lvl {question.difficulty}",
                style=random_button_style(), view=QuestionDescriptionView,
                view_params={'quiz': self.quiz, 'question': question}, question=question
            ))
        self.add_item(components.ViewButton(
            label='Back to categories', style=disnake.ButtonStyle.success,
            view=CategoryView, view_params={'quiz': self.quiz}
        ))

        return self


class QuestionDescriptionView(NoTimeoutView):

    def __init__(self, quiz: Quiz, question: Question):
        super(QuestionDescriptionView, self).__init__()
        self.quiz = quiz
        self.question = question

    @disnake.ui.button(
        label='Answer', style=disnake.ButtonStyle.green
    )
    async def answer(self, button: disnake.ui.Button, inter: disnake.MessageInteraction):
        button.disabled = True

        await inter.response.send_modal(
            components.AnswerModal(title='Your answer', components=[
                disnake.ui.TextInput(custom_id=self.get_id('answerInput'), label='answer')
            ], answer_button=button, quiz=self.quiz, answer_inter=inter, question=self.question)
        )
        await inter.edit_original_message(components=button)


class JudgeQuestionView(NoTimeoutView):

    def __init__(
            self, quiz: Quiz, question: Question,
            players_question_interaction: disnake.MessageInteraction
    ):
        super(JudgeQuestionView, self).__init__()
        self.quiz = quiz
        self.question = question
        self.player_question_interaction = players_question_interaction

    @disnake.ui.button(
        label='Skip this question', style=disnake.ButtonStyle.blurple
    )
    async def skip(self, _, inter: disnake.MessageInteraction):
        await inter.message.delete()
        self.quiz.remove_question(self.question)
        self.quiz.pop_empty_category()
        await self.player_question_interaction.message.delete()
        await self.player_question_interaction.send(
            content='Categories : ', view=CategoryView(quiz=self.quiz)
        )

    @disnake.ui.button(
        label='Restore answer button', style=disnake.ButtonStyle.secondary
    )
    async def restore(self, _, inter: disnake.MessageInteraction):
        await self.player_question_interaction.message.edit(components=button)
        await inter.response.defer(with_message=True)


class JudgeAnswerView(NoTimeoutView):

    def __init__(
            self, answer_button: disnake.ui.Button, quiz: Quiz,
            answer_inter: disnake.MessageInteraction, question: Question
    ):
        super(JudgeAnswerView, self).__init__()
        self.answer_button = answer_button
        self.answer_inter = answer_inter
        self.quiz = quiz
        self.question = question

    def answer_text(self, bad=False) -> str:
        bad = f"Right answer is  -  {self.question.correct}" if not bad else ""
        minus = '' if bad else '-'
        return f"Bad answer ! Your reward is {minus}{self.question.reward} \n{bad}"

    @disnake.ui.button(
        label='True', style=disnake.ButtonStyle.success
    )
    async def on_true(self, _, inter: disnake.MessageInteraction):
        await self.answer_inter.send(self.answer_text())
        self.quiz.players_scores[self.answer_inter.user] += self.question.reward
        await inter.message.delete()
        self.quiz.remove_question(p_question=self.question)
        self.quiz.pop_empty_category()
        await self.answer_inter.send(content='Categories : ', view=CategoryView(quiz=self.quiz))

    @disnake.ui.button(
        label='False', style=disnake.ButtonStyle.danger
    )
    async def on_false(self, _, inter: disnake.MessageInteraction):
        self.answer_button.disabled = False
        await self.answer_inter.send(self.answer_text(bad=True))
        self.quiz.players_scores[self.answer_inter.user] -= self.question.reward
        await inter.message.delete()
        await self.answer_inter.edit_original_message(components=self.answer_button)
