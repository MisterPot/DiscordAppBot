from typing import Any

import disnake

from .quiz import Question, Quiz
import quiz_application.view as views


class ViewButton(disnake.ui.Button):

    def __init__(
            self, view: Any, view_params: dict = None,
            inter_params: dict = None, *args, **kwargs
    ):
        super(ViewButton, self).__init__(*args, **kwargs)

        if inter_params is None:
            inter_params = {}

        if view_params is None:
            view_params = {}

        self.view_class = view
        self.view_class_params = view_params
        self.inter_params = inter_params

    async def show_view(self, interaction: disnake.MessageInteraction, **kwargs):
        await interaction.response.edit_message(view=self.view_class(**self.view_class_params), **kwargs)
        return interaction

    async def callback(self, interaction: disnake.MessageInteraction, /):
        await self.show_view(interaction, **self.inter_params)


class QuestionButton(ViewButton):

    def __init__(self, question: Question, *args, **kwargs):
        super(QuestionButton, self).__init__(*args, **kwargs)
        self.question = question

    async def callback(self, interaction: disnake.MessageInteraction, /):
        quiz = self.view_class_params['quiz']
        players_interaction = await self.show_view(interaction=interaction, content=self.question.description)
        await quiz.judge.send(content=f'Correct answer - {self.question.correct}', view=views.JudgeQuestionView(
            question=self.question, quiz=quiz, players_question_interaction=players_interaction
        ))


class AnswerModal(disnake.ui.Modal):

    def __init__(
            self, answer_button: disnake.ui.Button, quiz: Quiz,
            answer_inter: disnake.MessageInteraction, question: Question,
            *args, **kwargs
    ):
        super(AnswerModal, self).__init__(*args, **kwargs)
        self.quiz = quiz
        self.ans_button = answer_button
        self.answer_inter = answer_inter
        self.question = question

    async def callback(self, interaction: disnake.ModalInteraction, /) -> None:
        await interaction.send(
            content=f"Your answer is - {interaction.data.components[0]['components'][0]['value']}"
        )
        await self.quiz.judge.send(view=views.JudgeAnswerView(
            quiz=self.quiz, answer_button=self.ans_button, answer_inter=self.answer_inter, question=self.question
        ))

    async def on_timeout(self) -> None:
        self.ans_button.disabled = False
        self.answer_inter.message.edit(components=self.ans_button)
