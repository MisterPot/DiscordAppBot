from disnake.ext import commands
from .matcher import handle_message
import disnake


class CheckerCog(commands.Cog):

    def __init__(self, bot: commands.Bot):
        self.bot = bot

    @commands.Cog.listener()
    async def on_message(self, message: disnake.Message) -> None:
        await handle_message(message)
