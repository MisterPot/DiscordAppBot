import asyncio
import re
from abc import ABC, abstractmethod
from dataclasses import dataclass
from typing import List, Any, TypedDict
from urllib.error import HTTPError

from pytube import YouTube, Channel
import disnake


class InfoDict(TypedDict):
    vk: bool
    donationalerts: bool
    yandex: bool
    country: str


@dataclass
class ChannelInfo:
    about_html: str

    @property
    def country(self) -> str:
        raw_string = re.findall(r'"country":\{"simpleText":"\w+"}', self.about_html)
        if raw_string:
            return raw_string[0].split(':')[-1][1:-2]
        return 'Unknown'

    def have(self, domain: str) -> bool:
        return domain in self.about_html

    @property
    def info(self) -> InfoDict:
        return {
            'vk': self.have('vk.com'),
            'donationalerts': self.have('donationalerts.com'),
            'country': self.country,
            'yandex': self.have('yandex')
        }

    @property
    def valid(self) -> bool:
        return (
            (self.info['vk'] is False) and
            (self.info['donationalerts'] is False) and
            (self.info['yandex'] is False) and
            (self.info['country'] != 'Russia')
        )


class Replier(ABC):

    async def reply(self, message: disnake.Message, matches: List[str]) -> None:
        ...


async def safe_channel_info(channel: Channel, tries=3, timeout_seconds: int = 1) -> ChannelInfo:
    if tries == 0:
        raise RecursionError('The maximum number of attempts has been used')
    try:
        return ChannelInfo(about_html=channel.about_html)
    except HTTPError:
        await asyncio.sleep(timeout_seconds)
        return await safe_channel_info(channel=channel, tries=tries-1)


class RussianChannelReplier(Replier):

    async def reply(self, message: disnake.Message, matches: List[str]) -> Any:
        video = YouTube(r'https://' + matches[0])
        channel = Channel(video.channel_url)
        channel_info = await safe_channel_info(channel=channel)
        message_text = "It's a not terrorists 😇 But be careful"

        if not channel_info.valid:
            message_text = f"It's a fucking terrorists ⚠"

        await message.reply(content=message_text + f"\nCountry: {channel_info.info['country']}"
                                                   f"\nChannel url - {channel.channel_url}")


class Matcher(ABC):

    @abstractmethod
    async def handle_message(self, message: disnake.Message) -> None:
        ...


@dataclass
class PatternMatcher(Matcher):
    repliers: List[Replier]
    patterns: List[str]

    async def handle_message(self, message: disnake.Message) -> None:

        matches = []
        for pattern in self.patterns:
            matches += re.findall(pattern, message.content)

        if matches:
            await asyncio.gather(*[
                replier.reply(message, matches) for replier in self.repliers
            ])


MATCHERS = [
    PatternMatcher(
        patterns=[r'youtube\.com/watch\S+', r'youtu.be\.\S+'],
        repliers=[RussianChannelReplier()]
    ),
]


async def handle_message(message: disnake.Message) -> None:
    await asyncio.gather(*[
        matcher.handle_message(message) for matcher in MATCHERS
    ])
