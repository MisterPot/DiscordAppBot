
DERBY_CARS = [
    "Chili", "Malice", "Roamer",
    "Shaker", "Blaster XL", "Banger",
    "Splitter", "Switchblade", "Venom"
]

RACE_CARS = [
    "CTR", "Boxer", "Mad Rash",
    "Nevada", "Lancea", "Fortune",
    "Daytana", "Bullet", "Lentus",
    "Ventura", "Insetta"
]

STREET_RACING_CARS = [
    "Chili Pepper", "Scorpion", "Insetta Sport",
    "Sparrowhawk", "Crusader", "CTR Sport",
    "Vexter XS", "Speedshifter", "Canyon",
    "Terrator", "Sunray", "Speedevil",
    "Road King", "Bullet GT"
]

MEME_CARS = [
    "Pimpster", "Flatmobile", "Mob Car",
    "School bus", "Rocket", "Truck"
]

RACE_MAPS = {
    "ЛІС": [
        "Timberlands 1", "Timberlands 2", "Timberlands 3",
        "Pinegrove 1", "Pinegrove 2", "Pinegrove 3"
    ],
    "ПОЛЕ": [
        "Midwest Ranch 1", "Midwest Ranch 2", "Midwest Ranch 3",
        "Farmlands 1", "Farmlands 2", "Farmlands 3"
    ],
    "КАНАЛ": [
        "Water Canal 1", "Water Canal 2", "Water Canal 3"
    ],
    "МІСТО": [
        "City Central 1", "City Central 2", "City Central 3",
        "Downtown 1", "Downtown 2", "Downtown 3"
    ],
    "ПУСТЕЛЯ": [
        "Desert Oil Field", "Desert Scrap Yard", "Desert Town",
    ],
    "ГОНКИ": [
        "Riverbay Circuit 1", "Riverbay Circuit 2", "Riverbay Circuit 3",
        "Motor Raceway 1", "Motor Raceway 2", "Motor Raceway 3"
    ],
    "АРЕНА": [
        "Figure of Eight 1", "Triloop Special", "Speedbowl",
        "Sand Speedway", "Figure of Eight 2", "Crash Alley",
        "Speedway Left", "Speedway Right", "Speedway Special"
    ]
}

DERBY_MAPS = {
    "ДЕРБІ": [
        "Gas Station Derby", "Parking Lot Derby", "Skyscraper Derby",
        "Derby Bowl 1", "Derby Bowl 2", "Derby Bowl 3"
    ],

    "АРЕНА": [
        "Figure of Eight 1", "Triloop Special", "Speedbowl",
        "Sand Speedway", "Figure of Eight 2", "Crash Alley",
        "Speedway Left", "Speedway Right", "Speedway Special"
    ]
}

CAR_CATEGORIES = {
    "ALL": [*DERBY_CARS, *RACE_CARS, *STREET_RACING_CARS, *MEME_CARS],
    "STREET_RACING": STREET_RACING_CARS,
    "RACING": RACE_CARS,
    "DERBY": DERBY_CARS,
}

MODE_CATEGORIES = {
    "ALL": {**RACE_MAPS, **DERBY_MAPS},
    "RACING": RACE_MAPS,
    "DERBY": DERBY_MAPS
}
