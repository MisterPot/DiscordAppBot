from typing import Callable
from functools import wraps
import disnake


def on_randomizer_not_found(function: Callable) -> Callable:
    """
    If randomizer not found raises KeyError
    :param function:
    :return:
    """
    @wraps(function)
    async def argument_wrapper(
            self, inter: disnake.ApplicationCommandInteraction,
            name: str, *args, **kwargs
    ):
        if not self.randomizers.get(inter.user.id, {}).get(name):
            raise KeyError(f"Not randomizers with `name` - {name}")
        return await function(self, inter, name, *args, **kwargs)
    return argument_wrapper
