from random import choice
from typing import Callable

from ft_randomizer_application.constants import *


class Randomizer:
    CAR_CATEGORY = "ALL"
    MODE_CATEGORY = "RACING"

    def __init__(self, players_list: set):
        self.players = players_list
        self._player_formatter = lambda player: player
        self.exclude = []

    def __setattr__(self, key, value):
        if (key == "CAR_CATEGORY" and value in CAR_CATEGORIES) or \
           (key == "MODE_CATEGORY" and value in MODE_CATEGORIES) or \
           key in ['exclude', '_player_formatter', "players", 'player_formatter']:
            return object.__setattr__(self, key, value)
        raise KeyError("Can't set new attribute or attribute value is invalid")

    @property
    def player_formatter(self) -> Callable:
        """
        Needs to customize players view
        :return:
        """
        return self._player_formatter

    @player_formatter.setter
    def player_formatter(self, function: Callable) -> None:
        self._player_formatter = function

    def randomize_cars(self) -> str:
        """
        Choose random car for current car category for each player
        :return:
        """
        return "-" + "\n-".join([
            f'{self._player_formatter(player)} : {choice(CAR_CATEGORIES[self.CAR_CATEGORY])}'
            for player in self.players
        ])

    def info(self, rand_name: str) -> str:
        """
        Show short info about randomizer
        :param rand_name:
        :return:
        """
        user_string = ' '.join(self._player_formatter(pl) for pl in self.players)
        return f"{self._header(rand_name)}\n" \
               f" == Members: {user_string}"

    def _header(self, rand_name: str) -> str:
        return f" == Randomizer name: {rand_name} == \n" \
               f" == Car category: {self.CAR_CATEGORY} == \n" \
               f" == Mode category: {self.MODE_CATEGORY} == \n"

    def full_input(self, rand_name: str) -> str:
        """
        Returns full randomizer input
        :param rand_name: randomizer name
        :return:
        """
        return f"{self._header(rand_name)}\n" \
               f"{self.randomize_cars()}\n" \
               f" == Map : {self.randomize_map()} == "

    def randomize_map(self) -> str:
        """
        Choose random map from current mode category
        :return:
        """
        category = MODE_CATEGORIES[self.MODE_CATEGORY]
        map_type = choice(list(category.keys()))
        return f"{map_type} : {choice(category[map_type])}"
