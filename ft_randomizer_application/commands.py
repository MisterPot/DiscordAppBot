from typing import Dict, Callable, List

from ft_randomizer_application.constants import MODE_CATEGORIES, CAR_CATEGORIES
from ft_randomizer_application.randomizer import Randomizer
from ft_randomizer_application.utils import on_randomizer_not_found
import disnake
from disnake.ext import commands


async def opt_autocomplete(_, user_input: str, **kwargs):
    return [opt for opt in RandomizerCog.OPTS if user_input.lower() in opt.lower()]


async def opt_value_autocomplete(_, user_input: str, option: str, **kwargs):
    return [val for val in RandomizerCog.OPTS_VALUES[option] if user_input.lower() in val.lower()]


def randomizers_autocomplete(randomizers_dict: Dict[int, Dict[str, Randomizer]]) -> Callable:
    async def wrapper(inter: disnake.ApplicationCommandInteraction, name: str) -> List[str]:
        return [
            r_name for r_name in randomizers_dict.get(inter.user.id, {}).keys()
            if name.lower() in r_name.lower()
        ]
    return wrapper


class RandomizerCog(commands.Cog):
    OPTS = ['MODE_CATEGORY', 'CAR_CATEGORY']
    OPTS_VALUES = {
        "MODE_CATEGORY": list(MODE_CATEGORIES),
        "CAR_CATEGORY": list(CAR_CATEGORIES)
    }
    randomizers: Dict[int, Dict[str, Randomizer]] = {}

    def __init__(self, bot: commands.Bot):
        self.bot = bot

    @commands.slash_command(description="Create, menage and delete your randomizers")
    async def randomizer(self, inter: disnake.ApplicationCommandInteraction):
        pass

    @randomizer.sub_command(description="With this command you can change your randomizer settings")
    @on_randomizer_not_found
    async def settings(
            self, inter: disnake.ApplicationCommandInteraction,
            name: str = commands.Param(autocomplete=randomizers_autocomplete(randomizers_dict=randomizers)),
            option: str = commands.Param(autocomp=opt_autocomplete),
            opt_value: str = commands.Param(autocomp=opt_value_autocomplete)
    ):
        randomizer = self.randomizers[inter.user.id][name]
        old_value = getattr(randomizer, option)
        setattr(randomizer, option, opt_value)
        await inter.response.send_message(f'Changed option {option}: {old_value} -> {opt_value}')

    @randomizer.sub_command(description='You can create new randomizer')
    async def create(self, inter: disnake.ApplicationCommandInteraction, name: str):

        if not self.randomizers.get(inter.user.id):
            self.randomizers[inter.user.id] = {}

        if not self.randomizers[inter.user.id].get(name):
            self.randomizers[inter.user.id][name] = randomizer = Randomizer({inter.user})
            randomizer.player_formatter = lambda player: f" * <@{player.id}> * "

            await inter.response.send_message(randomizer.info(name))
        else:
            await inter.response.send_message('Randomizer already exists !')

    @randomizer.sub_command(description="Runs this randomizer, if you have him")
    @on_randomizer_not_found
    async def run(
            self, inter: disnake.ApplicationCommandInteraction,
            name: str = commands.Param(autocomplete=randomizers_autocomplete(randomizers_dict=randomizers))
    ):
        randomizer = self.randomizers[inter.user.id][name]
        randomizer.player_formatter = lambda player: f" | <@{player.id}>"

        await inter.response.send_message(randomizer.full_input(name))

    @randomizer.sub_command(description='Shows all your randomizers')
    async def all(self, inter: disnake.ApplicationCommandInteraction):
        randomizers = self.randomizers.get(inter.user.id)
        if randomizers:
            rand_string = ', '.join(map(lambda item: f'`{item}`', randomizers))
            return await inter.response.send_message(f'Your randomizers - {rand_string}')
        await inter.response.send_message(f'You haven\'t randomizers yet :(')

    @randomizer.sub_command(description='Add new member to you randomizer')
    @on_randomizer_not_found
    async def add(
            self, inter: disnake.ApplicationCommandInteraction,
            name: str = commands.Param(autocomplete=randomizers_autocomplete(randomizers_dict=randomizers)),
            member: disnake.User = commands.Param()
    ):
        self.randomizers[inter.user.id][name].players.add(member)
        await inter.response.send_message(f"Member added to randomizer `{name}` - <@{member.id}>")

    @randomizer.sub_command(description='Pops member from randomizer')
    @on_randomizer_not_found
    async def pop(
            self, inter: disnake.ApplicationCommandInteraction,
            name: str = commands.Param(autocomplete=randomizers_autocomplete(randomizers_dict=randomizers)),
            member: disnake.User = commands.Param()
    ):
        try:
            self.randomizers[inter.user.id][name].players.remove(member)
            await inter.response.send_message(f"User <@{member.id}> removed from randomizer `{name}`")
        except KeyError:
            await inter.response.send_message(f"User <@{member.id}> not in randomizer `{name}`")

    @randomizer.sub_command(description='Shows short info about randomizer')
    @on_randomizer_not_found
    async def info(
            self, inter: disnake.ApplicationCommandInteraction,
            name: str = commands.Param(autocomplete=randomizers_autocomplete(randomizers_dict=randomizers))
    ):
        randomizer = self.randomizers[inter.user.id][name]
        randomizer.player_formatter = lambda player: f" \\ <@{player.id}> \\"

        await inter.response.send_message(randomizer.info(name))

    @randomizer.sub_command(description='Delete your randomizer with that name')
    @on_randomizer_not_found
    async def delete(
            self, inter: disnake.ApplicationCommandInteraction,
            name: str = commands.Param(autocomplete=randomizers_autocomplete(randomizers_dict=randomizers))
    ):
        self.randomizers[inter.user.id].pop(name)
        await inter.response.send_message(f'Randomizer with name `{name}` was successfully deleted')
