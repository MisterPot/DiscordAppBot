import disnake
from music_application.controller import MusicController
from utils import add_validators, is_valid_url
from disnake.ext import commands
import os.path as path

controller = MusicController(ffmpeg_path=path.join(path.dirname(__file__), "ffmpeg.exe"))


class MusicCog(commands.Cog):
    
    def __init__(self, bot: commands.Bot):
        self.bot = bot

    @commands.slash_command(description='Control music application')
    async def music(self, inter: disnake.ApplicationCommandInteraction):
        pass

    @music.sub_command(description="Play music from passed url")
    @add_validators({
        "url": lambda url: is_valid_url(url)
    })
    async def play(self, inter: disnake.ApplicationCommandInteraction, url: str):
        await controller.play(inter=inter, url=url)

    @music.sub_command(description="Stops music if plays")
    async def stop(self, inter: disnake.ApplicationCommandInteraction):
        await controller.stop(inter=inter)

    @music.sub_command(description="Pauses music if plays")
    async def pause(self, inter: disnake.ApplicationCommandInteraction):
        await controller.pause(inter=inter)

    @music.sub_command(description='Resumes music if contains something to play')
    async def resume(self, inter: disnake.ApplicationCommandInteraction):
        await controller.resume(inter=inter)

    @music.sub_command(description='Play next song in playlist (only for playlists)')
    async def forward(self, inter: disnake.ApplicationCommandInteraction):
        await controller.forward(inter=inter)

    @music.sub_command(description='Retrying song which have this index in music order')
    @add_validators({
        "index": lambda index: isinstance(index, int)
    })
    async def retry(self, inter: disnake.ApplicationCommandInteraction, index: int):
        await controller.retry(inter=inter, index=index)

    @music.sub_command(description='Do loop for current song')
    @add_validators({
        "is_looping": lambda is_looping: isinstance(is_looping, bool)
    })
    async def loop(self, inter: disnake.ApplicationCommandInteraction, is_looping: bool = True):
        await controller.loop(inter=inter, is_looping=is_looping)

    @music.sub_command(description='Shows which music playing now')
    async def current(self, inter: disnake.ApplicationCommandInteraction):
        url = controller.plays_now.original_url
        await inter.response.send_message(
            "Now playing - " + url if url else "Nothing plays now"
        )
