from __future__ import annotations
import os.path as path
import os
from asyncio import Task
from typing import List, Callable
import asyncio
import logging
import discord
import discord.utils
import disnake

from collections import deque

from .downloader import get_downloader_by_url, Downloader
from .utils import void, set_up_logger, do_response
from .utils import transform_to_client, check_member

controller_logger = logging.getLogger('MusicController')
set_up_logger(controller_logger)

music_logger = logging.getLogger('MusicFile')
set_up_logger(music_logger)

playlist_logger = logging.getLogger('PlayList')
set_up_logger(playlist_logger)

controller_checker = check_member([
    lambda self, inter: self.play_member is None,
    lambda self, inter: inter.user == self.play_member,
    lambda self, inter: disnake.utils.get(inter.user.roles, name="Адмін")
])


class MusicController:

    def __init__(self, ffmpeg_path: str):
        self.cache = {}
        self.play_order = []
        self.ffmpeg_path = ffmpeg_path
        self.current_sequence: deque = deque()
        self.plays_now: MusicFile | None = None
        self.play_member = None
        self.root_dir = path.dirname(__file__)
        self.temp = self.__create_temp_if_not_exists()
        self._is_loop = False
        controller_logger.info('Controller was setup')

    def __create_temp_if_not_exists(self) -> str:
        """
        Help function for temp directory
        :return:
        """
        temp_path = path.join(self.root_dir, 'temp')
        if not path.exists(temp_path):
            controller_logger.info(f'Temp dir not exists, creating path - {temp_path}')
            os.mkdir(temp_path)
        return temp_path

    def in_music_ends(
            self, client: discord.VoiceClient,
            url: str, inter: disnake.ApplicationCommandInteraction
    ) -> Callable:
        """
        When music was stop playing himself, this function will be activated
        :param inter:
        :param client: connected voice client
        :param url: fetch url
        :return:
        """

        def wrapper(_):
            if client.is_connected() and not client.is_playing():
                if self._is_loop:
                    controller_logger.info(f'Looping music with url - {url}')
                    asyncio.run(self.play(inter=inter, client=client, url=url))

                if self.current_sequence:
                    music_file = self.current_sequence.popleft()
                    controller_logger.info(f"Sequence is active - {self.current_sequence}")
                    controller_logger.info(f'Try to play next song - {music_file}')
                    asyncio.run(self.play(inter=inter, client=client, music_file=music_file, url=url))
            self.play_member = None

        return wrapper

    def playlist_on_item_load(
            self, client: discord.VoiceClient,
            inter: disnake.ApplicationCommandInteraction
    ) -> Callable:
        """
        If to command /play was passed url, which means playlist,
        this function will be activated on each iteration of loading playlist
        :param inter:
        :param client: active voice client
        :return:
        """
        self.current_sequence = deque()
        first = True

        async def wrapper(_, item: MusicFile):
            nonlocal first
            self.current_sequence.append(item)
            if first and not client.is_playing():
                await self.forward(inter=inter, client=client)
                first = False

        return wrapper

    async def play_post_load(
            self, client: discord.VoiceClient, url: str,
            inter: disnake.ApplicationCommandInteraction
    ) -> None:
        """
        Loads files from cache or downloads from provider url
        and play them, if can
        :param inter:
        :param client: active voice client
        :param url: fetch url
        :return:
        """
        if PlayList.is_playlist(url):
            if self.cache.get(url):
                controller_logger.info(f'Playlist in cache, playing - {url}')
                self.current_sequence = deque(self.cache[url])
                ms_file = self.current_sequence.popleft()

                return await self.play(
                    inter=inter, client=client, url=ms_file.original_url,
                    music_file=ms_file
                )
            controller_logger.info(f'Playlist not in cache, downloading - {url}')

            playlist = PlayList(url)
            playlist.on_one_ready = self.playlist_on_item_load(client=client, inter=inter)
            await playlist.fetch_as_completed(self.temp)

            for mf in playlist.music_files:
                self.cache[mf.original_url] = mf
            self.cache[url] = playlist.music_files

        else:
            if not self.cache.get(url):
                controller_logger.info(f"MusicFile not in cache, downloading - {url}")
                self.cache[url] = await MusicFile.create_music_file(
                    url=url, filedir=self.temp
                )
            await self.play(
                inter=inter, client=client, url=url,
                music_file=self.cache[url]
            )

    @controller_checker
    @transform_to_client
    async def play(
            self, inter: disnake.ApplicationCommandInteraction, client: discord.VoiceClient,
            url: str, music_file: MusicFile = None
    ) -> None:
        """
        Play music in chanel, where is message author.
        Stops music , if playing
        :param inter:
        :param client:
        :param url:
        :param music_file:
        :return:
        """
        await do_response(inter, f'Try to play - {url}')
        if client.is_playing():
            client.stop()
        if not music_file:
            controller_logger.info(f'No `music_file`, try to download or check cache for url - {url}')
            await self.play_post_load(client=client, url=url, inter=inter)
        else:
            controller_logger.info(f'Start playing - {music_file}')
            self.play_order.append(url)
            self.plays_now = music_file
            client.play(
                disnake.FFmpegPCMAudio(source=music_file.filepath, executable=self.ffmpeg_path),
                after=self.in_music_ends(client=client, url=url, inter=inter)
            )

    @controller_checker
    @transform_to_client
    async def pause(
            self, inter: disnake.ApplicationCommandInteraction,
            client: discord.VoiceClient
    ) -> None:
        """
        Just set pause to music
        :param inter:
        :param client:
        :return:
        """
        if client.is_playing():
            controller_logger.info('Controller paused')
            client.pause()
            await do_response(inter, 'Music paused')
        else:
            await do_response(inter, 'Music already in pause')

    @controller_checker
    @transform_to_client
    async def resume(
            self, inter: disnake.ApplicationCommandInteraction,
            client: discord.VoiceClient
    ) -> None:
        """
        Same as pause , but resumes music
        :param inter:
        :param client:
        :return:
        """
        if client.is_paused():
            controller_logger.info('Controller stopped')
            client.resume()
            await do_response(inter, 'Resuming music')
        else:
            await do_response(inter, "Music already plays")

    @controller_checker
    @transform_to_client
    async def stop(
            self, inter: disnake.ApplicationCommandInteraction,
            client: discord.VoiceClient
    ) -> None:
        if client.is_connected():
            controller_logger.info('Controller stopped')
            client.stop()
            await do_response(inter, 'Music is stopped')
            await client.disconnect()
        else:
            await do_response(inter, 'Music already stopped')
        self.current_sequence = deque()
        self.play_member = None

    @controller_checker
    @transform_to_client
    async def forward(
            self, inter: disnake.ApplicationCommandInteraction,
            client: discord.VoiceClient
    ) -> None:
        """
        Plays next sound , if it's playlist
        else nothing
        :param inter:
        :param client:
        :return:
        """
        if self.current_sequence:
            music_file = self.current_sequence.popleft()
            controller_logger.info(f'Playing next song - {music_file}')
            await self.play(inter=inter, client=client, music_file=music_file, url=music_file.original_url)
        else:
            await do_response(inter, 'No playlist to forwarding music')

    @controller_checker
    @transform_to_client
    async def back(self, client: discord.VoiceClient) -> None:
        pass

    @controller_checker
    @transform_to_client
    async def retry(
            self, inter: disnake.ApplicationCommandInteraction,
            client: discord.VoiceClient, index: int
    ) -> None:
        if not index > len(self.play_order) - 1:
            url = self.play_order[index]
            controller_logger.info(f'Retrying song - {url}')
            await do_response(inter, f'Retrying song - {url}')
            await self.play(inter=inter, client=client, url=url)
        else:
            await do_response(inter, 'Invalid index')

    @controller_checker
    async def loop(
            self, inter: disnake.ApplicationCommandInteraction,
            is_looping: bool = True
    ) -> None:
        controller_logger.info(f'Looping now is - {is_looping}')
        self._is_loop = is_looping
        await do_response(inter, f'Looping now is - {is_looping}')

    def __del__(self):
        os.chdir(self.temp)
        for file in os.listdir():
            os.remove(file)


class MusicInterface:

    @classmethod
    def is_playlist(cls, url: str) -> bool:
        downloader = get_downloader_by_url(url)
        return downloader.is_playlist(url)


class MusicFile(MusicInterface):

    def __init__(self, url: str, name: str, filepath: str):
        self.name = name
        self.original_url = url
        self.filepath = filepath

    @classmethod
    async def create_music_file(cls, url: str, filedir: str, downloader: Downloader = None) -> MusicFile:
        """
        Creates MusicFiles object
        :param downloader:
        :param url: url to fetch this file or YouTube url
        :param filedir: where this file will be saved
        :return:
        """
        downloader = downloader or get_downloader_by_url(url)

        out_path = await downloader.download_one(url=url, save_path=filedir)
        music_logger.info(f'Byte content was read - {url}')
        music_logger.info(f"File saved - {out_path}")

        file = os.path.basename(out_path)

        return MusicFile(name=file, url=url, filepath=out_path)

    def __repr__(self):
        return f"<MusicFile original_url = {self.original_url} | name = {self.name}>"


class PlayList(MusicInterface):

    def __init__(self, original_url: str):
        self.original_url = original_url
        self.downloader = get_downloader_by_url(original_url)
        self.music_files = []
        self._current_music_file = None
        self._on_one_ready = void
        self._on_all_ready = void

    async def __get_task_list(self, filepath: str) -> List[Task]:
        """
        Starts creating music files in background and
        returns this task list
        :param filepath:
        :return:
        """
        playlist_logger.info(f'Starts download playlist - {self.original_url}')
        return [
            asyncio.create_task(MusicFile.create_music_file(
                url=url, filedir=filepath
            ))
            for url in await self.downloader.get_playlist(self.original_url)
        ]

    async def fetch_all(self, filepath: str) -> None:
        """
        Download and save all files in one time
        :param filepath: path to save files
        :return:
        """
        self.music_files = await asyncio.gather(*(await self.__get_task_list(filepath)))
        await self._on_all_ready(self, self.music_files)
        playlist_logger.info(f'Playlist completed - {self.original_url}')

    async def fetch_as_completed(self, filepath: str) -> None:
        """
        Download and save files , which will does faster,
        then another files
        :param filepath: path to save files
        :return:
        """
        tasks = await self.__get_task_list(filepath)

        for task in asyncio.as_completed(tasks):
            completed_task = await task

            self.music_files.append(completed_task)
            playlist_logger.info(f'Item ready - {completed_task}')
            await self._on_one_ready(self, completed_task)

        playlist_logger.info(f'Playlist completed - {self.original_url}')

    @property
    def on_one_ready(self) -> Callable:
        """
        Can be replaced to another function.
        Handles for `fetch_as_completed` function
        :return:
        """
        return self._on_one_ready

    @on_one_ready.setter
    def on_one_ready(self, function: Callable):
        self._on_one_ready = function

    @property
    def on_all_ready(self) -> Callable:
        """
        Can be replaced to another function.
        Handles for `fetch_all` function
        :return:
        """
        return self._on_all_ready

    @on_all_ready.setter
    def on_all_ready(self, function: Callable):
        self._on_all_ready = function

    def __repr__(self):
        return '[ ' + ' , '.join(self.music_files) + ' ]'
