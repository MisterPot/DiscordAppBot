import logging
from logging import Logger
from typing import Callable, List
from functools import wraps

from discord import VoiceClient
import disnake


def transform_to_client(function: Callable) -> Callable:
    """
    Try to get a valid voice client and pass him to function
    :param function:
    :return:
    """
    # noinspection PyTypeChecker
    @wraps(function)
    async def arg_wrapper(
            self, inter: disnake.ApplicationCommandInteraction,
            client: VoiceClient = None, *args, **kwargs
    ) -> Callable:
        if client is None:
            clients: VoiceClient = inter.client.voice_clients
            if not clients:
                client = await inter.user.voice.channel.connect()
            else:
                client = inter.client.voice_clients[0]
        return await function(self, inter, client, *args, **kwargs)
    return arg_wrapper


def check_member(check_functions_list: List[Callable]) -> Callable:
    """
    Takes a list of check functions to check user
    :param check_functions_list: function, which takes 2 args - relative object and `ApplicationCommandInteraction`
    :return:
    """
    def func_wrapper(function: Callable) -> Callable:
        # noinspection PyTypeChecker
        @wraps(function)
        async def arg_wrapper(self, inter: disnake.ApplicationCommandInteraction, *args, **kwargs):
            if not inter.user.voice:
                return await inter.response.send_message('You must be in voice channel')

            if any(func(self, inter) for func in check_functions_list):
                self.play_member = inter.user
                return await function(self, inter, *args, **kwargs)

            else:
                await inter.response.send_message("You can't do this")
        return arg_wrapper
    return func_wrapper


async def void(*args, **kwargs):
    """
    Function which do nothing
    :param args:
    :param kwargs:
    :return:
    """
    pass


class MutableGenerator(list):
    """
    Needs to add and iterate in runtime list
    """
    index = 0

    def __next__(self):
        if self.index == len(self):
            self.index = 0

        item = self[self.index]
        self.index += 1
        return item

    def __bool__(self):
        if len(self) == 0 or self.index == len(self):
            return False
        return True


def set_up_logger(logger: Logger) -> None:
    """
    Setups loggers for default discord logger view
    :param logger:
    :return:
    """
    handler = logging.StreamHandler()
    handler.setLevel(logging.DEBUG)
    handler.setFormatter(logging.Formatter(
        "[%(asctime)s] [%(levelname)s    ] %(name)s: %(message)s", datefmt='%Y-%m-%d,%H:%M:%S'
    ))
    logger.setLevel(logging.DEBUG)
    logger.addHandler(handler)


async def do_response(inter: disnake.ApplicationCommandInteraction, message: str):
    """
    Sends response, if not responded to request
    :param inter:
    :param message:
    :return:
    """
    if not inter.response.is_done():
        await inter.response.send_message(message)
