from __future__ import annotations

from abc import ABC, abstractmethod
from typing import Any, Iterable, List, Callable
from urllib.parse import urlparse
import asyncio
import shutil
import tempfile
import os

from bs4 import BeautifulSoup
from pytube import YouTube, Playlist, Search, Stream
import requests
import platform


system = platform.system()


if system == 'Linux':
    DEFAULT_SAVE_PATH = '~/Downloads'

elif system == 'Windows':
    userprofile = os.environ['USERPROFILE']
    DEFAULT_SAVE_PATH = os.path.join(userprofile, 'Downloads')


class Downloader(ABC):

    @abstractmethod
    async def download_one(self, url: str, save_path: str = DEFAULT_SAVE_PATH) -> str:
        ...

    @abstractmethod
    async def download_playlist(self, url: str, save_path: str = DEFAULT_SAVE_PATH) -> str:
        ...

    @abstractmethod
    def is_playlist(self, url: str) -> bool:
        ...

    @staticmethod
    @abstractmethod
    async def get_playlist(url: str) -> Iterable[str]:
        ...


async def use_executor(func: Callable) -> Any:
    return await asyncio.get_event_loop().run_in_executor(None, func)


class YoutubeDownloader(Downloader):

    def __init__(self):
        self.__percentage = 0

    @property
    def percentage(self) -> float:
        return float("{:.2f}".format(self.__percentage))

    @percentage.setter
    def percentage(self, value):
        self.__percentage = value

    @staticmethod
    async def get_song_stream(youtube: YouTube) -> Stream:
        return await use_executor(
            lambda: youtube.streams.filter(only_audio=True).first()
        )

    @staticmethod
    async def to_mp3(filepath: str, tries=3, timeout_seconds=0.5) -> str:

        if not tries > 0:
            raise RecursionError(f'Max tries reached for path - {filepath}')

        filename, ext = os.path.splitext(filepath)
        new_save_path = filename + '.mp3'

        try:
            os.rename(filepath, new_save_path)
        except FileExistsError:
            os.remove(filepath)
        except FileNotFoundError:
            pass
        except PermissionError:  # Can't rename, because another process reads file
            await asyncio.sleep(timeout_seconds)
            await YoutubeDownloader.to_mp3(filepath=filepath, tries=tries - 1)

        return new_save_path

    def set_percentage(self, value: float, add=True):
        if add:
            self.percentage += value
        else:
            self.percentage = value

    @staticmethod
    def add_author(path: str, author: str) -> str:
        base, ext = os.path.splitext(path)
        new_path = f"{base} - {author}{ext}"
        os.rename(path, new_path)
        return new_path

    async def download_one(
            self,
            url: str,
            save_path: str = DEFAULT_SAVE_PATH,
            percentage: int = 100,
            add_author: bool = True,
            clear_after=False
    ) -> str:
        percentage_per_action = percentage / 4
        youtube = YouTube(url)
        self.set_percentage(percentage_per_action)

        song_stream = await self.get_song_stream(youtube)
        self.set_percentage(percentage_per_action)

        saved_path = await use_executor(lambda: song_stream.download(save_path))
        self.set_percentage(percentage_per_action)

        renamed_path = await self.to_mp3(saved_path, tries=5)
        self.set_percentage(percentage_per_action)

        if clear_after:
            self.set_percentage(0, add=False)

        if add_author:
            renamed_path = self.add_author(renamed_path, youtube.author)

        return renamed_path

    @staticmethod
    async def get_playlist(url: str) -> Iterable[str]:
        playlist = Playlist(url)
        return await use_executor(lambda: playlist.video_urls)

    @staticmethod
    async def get_playlist_name(url: str) -> str:
        playlist = Playlist(url)
        return await use_executor(lambda: playlist.title)

    def is_playlist(self, url: str) -> bool:
        parsed = urlparse(url=url)
        return parsed.path == '/playlist'

    async def download_playlist(self, url: str, save_path: str = DEFAULT_SAVE_PATH) -> str:
        save_dir = os.path.join(save_path, self.get_playlist_name(url=url) or 'UnknownPlaylist')

        if not os.path.exists(save_dir):
            os.mkdir(save_dir)

        video_urls = await self.get_playlist(url=url)
        percent_per_song = 95 / len(video_urls)

        await asyncio.gather(*[
            self.download_one(url=s_url, save_path=save_dir, percentage=percent_per_song)
            for s_url in video_urls
        ])

        self.set_percentage(0, add=False)
        return save_dir


class SpotifySongItem:

    def __init__(self, author: str, song_name: str):
        self.author = author
        self.song_name = song_name

    @classmethod
    def create_from_playlist(cls, soup: BeautifulSoup) -> List[SpotifySongItem]:
        songs_tags = soup.select('[href^="https://open.spotify.com/track/"]')
        return [
            cls(
                author=tag.parent.next_sibling.text,
                song_name=tag.text
            ) for tag in songs_tags
        ]

    @classmethod
    def create_from_single(cls, soup: BeautifulSoup):
        song_name = soup.find(attrs={'data-testid': "track-entity-title"}).text
        author = soup.find(attrs={"data-testid": "track-entity-metadata"}).next.text
        return cls(
            song_name=song_name,
            author=author
        )

    @property
    def full_name(self) -> str:
        return f"{self.author} - {self.song_name}"

    async def get_video_url(self) -> YouTube:
        search = Search(self.full_name)
        return await use_executor(lambda: search.results[0].watch_url)


class SpotifyDownloader(Downloader):

    def __init__(self):
        self.ytd = YoutubeDownloader()

    @property
    def percentage(self) -> int:
        return self.ytd.percentage

    @staticmethod
    async def request_to_soup(url: str) -> BeautifulSoup:
        response = await use_executor(lambda: requests.get(url))
        return BeautifulSoup(response.content, 'html.parser')

    @staticmethod
    async def get_playlist(soup_or_url: BeautifulSoup or str) -> Iterable[str]:
        soup = soup_or_url
        if isinstance(soup_or_url, str):
            soup = await SpotifyDownloader.request_to_soup(url=soup_or_url)
        items = SpotifySongItem.create_from_playlist(soup=soup)
        return await asyncio.gather(*[item.get_video_url() for item in items])

    @staticmethod
    def get_playlist_name(soup: BeautifulSoup) -> str:
        return soup.find('title').text.replace(' ', '').split('-')[0]

    def is_playlist(self, url: str) -> bool:
        parsed = urlparse(url=url)
        return parsed.path.startswith('/playlist/')

    async def download_playlist(
            self,
            url: str,
            save_path: str = DEFAULT_SAVE_PATH,
            clear_after=False
    ) -> str:
        soup = await self.request_to_soup(url=url)
        self.ytd.set_percentage(1)

        playlist = await self.get_playlist(soup=soup)
        self.ytd.set_percentage(1.5)

        playlist_name = self.get_playlist_name(soup=soup)
        self.ytd.set_percentage(.5)

        new_save_path = os.path.join(save_path, playlist_name)
        percentage_per_song = 95 / len(playlist)

        if not os.path.exists(new_save_path):
            os.mkdir(new_save_path)

        await asyncio.gather(*[
            self.ytd.download_one(
                url=url,
                save_path=new_save_path,
                percentage=percentage_per_song
            ) for url in playlist
        ])

        if clear_after:
            self.ytd.set_percentage(0, add=False)

        return new_save_path

    async def download_one(
            self,
            url: str,
            save_path: str = DEFAULT_SAVE_PATH,
            add_author: bool = True
    ) -> str:
        soup = await self.request_to_soup(url=url)
        item = SpotifySongItem.create_from_single(soup=soup)
        song_url = await item.get_video_url()

        with tempfile.TemporaryDirectory(dir=save_path) as tmp:
            filepath = await self.ytd.download_one(
                url=song_url,
                save_path=tmp,
                add_author=add_author
            )
            try:
                shutil.move(filepath, save_path)
            except shutil.Error:
                os.remove(filepath)

        self.ytd.set_percentage(0, add=False)
        filename = os.path.basename(filepath)
        return os.path.join(save_path, filename)


DOWNLOADERS_MATCHES = {
    "youtube.com": YoutubeDownloader(),
    'spotify.com': SpotifyDownloader(),
    'youtu.be': YoutubeDownloader()
}


class ServiceNotFoundError(Exception):
    ...


def get_downloader_by_url(url: str) -> Downloader:
    for service_host, downloader in DOWNLOADERS_MATCHES.items():
        if service_host in url:
            return downloader

    proto, path = url.split('//')
    domain, query = path.split('/', maxsplit=1)

    raise ServiceNotFoundError(f'No downloader for such service : {domain}')


async def auto_download(
        downloader: Downloader,
        url: str,
        save_path: str = DEFAULT_SAVE_PATH
) -> str:
    if downloader.is_playlist(url=url):
        return await downloader.download_playlist(url=url, save_path=save_path)
    return await downloader.download_one(url=url, save_path=save_path, clear_after=True)
