import disnake
from disnake.ext import commands
import rsp_application.components as components
import rsp_application.game as g_module
import rsp_application.statistic as st


class RspCog(commands.Cog):

    def __init__(self, bot: commands.Bot):
        self.bot = bot
        self.statistic = st.Statistic()

    @commands.slash_command()
    async def rsp(self, inter: disnake.ApplicationCommandInteraction):
        pass

    @rsp.sub_command(description='Beats the player')
    async def beat(self, inter: disnake.ApplicationCommandInteraction, member: disnake.Member):

        battle = self.statistic.find_battle(inter.user, member)

        if not battle:
            battle = st.Battle(inter.user.id, member.id)
            self.statistic.players_games.append(battle)

        game = g_module.Game(p1=inter.user, p2=member, battle=battle)
        game.state[inter.user.id] = {'interaction': inter}

        await inter.response.send_message(
            f"Hey <@{member.id}>, <@{inter.user.id}> beats you to Rock, Paper, Scissors :D\n"
            f"Are you afraid or will fight ?", view=components.BeatView(game=game), delete_after=60
        )

    @rsp.sub_command(description='You can see your or all game statistic')
    async def statistic(
            self, inter: disnake.ApplicationCommandInteraction,
            surface: str = commands.Param(choices=['my', 'battles'])
    ):
        if surface == 'battles':
            await inter.response.send_message(
                '\n'.join(map(
                    lambda item: f" - {str(item)}",
                    self.statistic.players_games
                )), ephemeral=True
            )
        elif surface == 'my':
            score = self.statistic.get_all_score_one_player(p=inter.user)
            await inter.response.send_message(
                f"Your wins - {score['wins']} | Your defeats - {score['defeats']}", ephemeral=True
            )
