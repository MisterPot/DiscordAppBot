import disnake

import rsp_application.game as g_module


class GameView(disnake.ui.View):

    def __init__(self, game: g_module.Game):
        super(GameView, self).__init__()
        self.game = game


class SelectView(GameView):

    @disnake.ui.select(custom_id='select', options=[
        disnake.SelectOption(label='🪨 Rock', value=g_module.BattleItems.ROCK.name),
        disnake.SelectOption(label="✂ Scissors", value=g_module.BattleItems.SCISSORS.name),
        disnake.SelectOption(label='🧻 Paper', value=g_module.BattleItems.PAPER.name)
    ])
    async def select(self, _, inter: disnake.MessageInteraction):
        self.game.state[inter.user.id]['value'] = inter.values[0] if inter.values else None


class BeatView(GameView):

    @disnake.ui.button(
        label='Beat', style=disnake.ButtonStyle.green
    )
    async def beat(self, _, interaction: disnake.MessageInteraction):

        if interaction.user == self.game.p2:
            self.game.state[interaction.user.id] = {'interaction': interaction}
            await self.game.wait_results()

        elif interaction.user != self.game.p2:
            await interaction.response.send_message(ephemeral=True, content='You are not a duel player')

        else:
            await interaction.response.send_message(ephemeral=True, content='You can\'t duel yourself :D')
