from __future__ import annotations
import asyncio
from typing import Dict, Any

import disnake
import dataclasses

import rsp_application.components as components
import rsp_application.statistic as st


class Emoji:
    SCISSORS = '✂'
    ROCK = ":rock:"
    PAPER = '🧻'


@dataclasses.dataclass
class BattleItem:
    name: str | None
    good_against: str | None
    bad_against: str | None

    def __truediv__(self, other: BattleItem):

        if self is other:
            return

        if not other or not self:
            return other or self

        if isinstance(other, BattleItem):
            if self.good_against == other.name:
                return self
            return other

    def __bool__(self):
        return self.name != 'None'


class BattleItems:
    SCISSORS = BattleItem(name='SCISSORS', good_against='PAPER', bad_against='ROCK')
    ROCK = BattleItem(name='ROCK', good_against='SCISSORS', bad_against='PAPER')
    PAPER = BattleItem(name='PAPER', good_against='ROCK', bad_against='SCISSORS')


@dataclasses.dataclass
class Game:
    p1: disnake.Member
    p2: disnake.Member
    battle: st.Battle
    state: Dict[int, Dict[str, Any]] = dataclasses.field(default_factory=dict)

    async def wait_results(self) -> None:
        """
        Send selects and waiting for 3 seconds before show results
        :return:
        """
        main_interaction: disnake.ApplicationCommandInteraction = self.state[self.p1.id]['interaction']
        await self.send_selects()

        for i in range(3, 0, -1):
            await asyncio.sleep(1)
            await main_interaction.edit_original_message(content=f'{i} seconds left to results, hurry !', components=[])

        await main_interaction.edit_original_message(content=self.get_winners(), components=[])

    async def send_selects(self) -> None:
        """
        Send personal select input to both players of this battle
        :return:
        """
        for p_data in self.state.values():
            await p_data['interaction'].send(
                ephemeral=True, view=components.SelectView(self)
            )

    def get_winners(self) -> str:
        """
        Returns resulting string of the battle and updates battles statistics
        :return:
        """
        p1_value, p2_value = [
            getattr(
                BattleItems, data.get('value', ''),
                BattleItem(name='None', good_against=None, bad_against=None)
            ) for data in self.state.values()
        ]
        winner_line = f"<@{self.p1.id}> {getattr(Emoji, p1_value.name, 'Nothing')} " \
                      f"VS {getattr(Emoji, p2_value.name, 'Nothing')} <@{self.p2.id}>\n"

        winner_item = p1_value / p2_value
        winner_em = getattr(Emoji, winner_item.name if winner_item else "", None)

        if winner_em is None:
            winner_line += "Draw !"
            return winner_line

        if winner_item == p1_value:
            self.battle.s1 += 1
            winner_line += f'Winner is <@{self.p1.id}>'

        elif winner_item == p2_value:
            self.battle.s2 += 1
            winner_line += f'Winner is <@{self.p2.id}> !'

        return winner_line
