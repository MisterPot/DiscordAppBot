from __future__ import annotations
import dataclasses
from typing import List, Dict

import disnake


@dataclasses.dataclass
class Battle:
    p1_id: int
    p2_id: int
    s1: int = 0
    s2: int = 0

    def get_p_score(self, p_id: int) -> Dict[str, int]:
        if self.p1_id == p_id:
            return {'defeats': self.s2, 'wins': self.s1}

        elif self.p2_id == p_id:
            return {'defeats': self.s1, 'wins': self.s2}

        raise KeyError(f"No player with such id - {p_id}")

    def __eq__(self, other: Battle):
        return (
                (other.p2_id == self.p2_id or other.p2_id == self.p1_id)
                and (other.p1_id == self.p1_id or other.p1_id == self.p2_id)
        )

    def __str__(self):
        return f"<@{self.p1_id}> {self.s1} | {self.s2} <@{self.p2_id}>"


@dataclasses.dataclass
class Statistic:
    players_games: List[Battle] = dataclasses.field(default_factory=list)

    def find_battle(self, p1: disnake.Member, p2: disnake.Member) -> Battle | None:
        """
        Try to find battle with current players
        If not found - None
        :param p1:
        :param p2:
        :return:
        """
        new_battle = Battle(p1_id=p1.id, p2_id=p2.id)
        for battle in self.players_games:
            if battle == new_battle:
                return battle

    def find_player_in_battles(self, p: disnake.Member) -> List[Battle]:
        """
        Find all battles in which this player was played
        :param p:
        :return:
        """
        return list(filter(
            lambda battle: battle.p1_id == p.id or battle.p2_id == p.id,
            self.players_games
        ))

    def get_all_score_one_player(self, p: disnake.Member) -> Dict[str, int]:
        """
        Returns total wins and defeat of current player
        :param p:
        :return:
        """
        wins = defeats = 0

        for battle in self.find_player_in_battles(p=p):
            score = battle.get_p_score(p_id=p.id)
            wins += score['wins']
            defeats += score['defeats']

        return {'wins': wins, 'defeats': defeats}

    def add_battle(self, p1: disnake.Member, p2: disnake.Member) -> None:
        """
        Add new battle to statistic if battle not exists in statistic
        If battle have reversed players order it's not be added
        :param p1:
        :param p2:
        :return:
        """
        if not self.find_battle(p1=p1, p2=p2):
            self.players_games.append(Battle(p1_id=p1.id, p2_id=p2.id))
