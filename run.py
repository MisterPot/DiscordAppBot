from ui.controller import Controller
from ui.model import Model
from ui.view import View


if __name__ == '__main__':
    model = Model()
    view = View(model=model)
    controller = Controller(model=model, view=view)
    controller.run()