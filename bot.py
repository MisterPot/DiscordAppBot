import ast
import asyncio
import platform
import disnake
from disnake.ext import commands
from disnake.ext.commands import CommandInvokeError
from bot_token import token
from utils import load_applications

# Windows have some problems if async code ends, therefore adding next code
if platform.platform().startswith('Windows'):
    asyncio.set_event_loop_policy(asyncio.WindowsSelectorEventLoopPolicy())

event_loop = asyncio.get_event_loop_policy().new_event_loop()
asyncio.set_event_loop(event_loop)

# Initialize bot and permissions
intents = disnake.Intents.default()
intents.voice_states = True
intents.messages = True
intents.message_content = True

bot = commands.Bot(command_prefix='/', intents=intents, sync_commands=True)
bot.guilds


async def on_error(inter: disnake.ApplicationCommandInteraction, error: CommandInvokeError):
    str_err = str(error.original)
    # noinspection PyBroadException
    try:
        final_err = ast.literal_eval(str_err)
    except Exception:
        final_err = str_err
    if not inter.response.is_done():
        await inter.response.send_message(final_err)
    raise error


bot.on_slash_command_error = on_error


if __name__ == "__main__":
    # Load apps from config file
    load_applications(bot)

    bot.run(token)
